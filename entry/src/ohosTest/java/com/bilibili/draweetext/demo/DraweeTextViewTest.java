/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.draweetext.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.bilibili.draweetext.DraweeSpan;
import com.bilibili.draweetext.DraweeTextView;
import com.bilibili.draweetext.demo.util.ResUtil;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;

import org.junit.Before;
import org.junit.Test;

/**
 * TestCases for DraweeTextView class
 */
public class DraweeTextViewTest {
    private DraweeTextView mText;

    private Context context;

    /**
     * test case setup
     */
    @Before
    public void setup() {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        mText = new DraweeTextView(context);
    }

    /**
     * test case SetAroundElements
     */
    @Test
    public void tesSetAroundElements() {
        PixelMapElement pixelMapElement1 = ResUtil.getPixelMapDrawable(context, ResourceTable.Media_icon);
        PixelMapElement pixelMapElement2 = ResUtil.getPixelMapDrawable(context, ResourceTable.Media_icon_2);
        DraweeSpan span1 = new DraweeSpan.Builder(
            "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(100, 100)
            .setPlaceHolderImage(pixelMapElement1)
            .build();

        DraweeSpan span2 = new DraweeSpan.Builder(
            "https://i0.hdslb.com/bfs/vip/458982f20f0b7dc68c0ddac89f51ecb7c3d16a83.png").setLayout(200, 200)
            .setPlaceHolderImage(pixelMapElement2)
            .build();
        mText.setText("HelloWorld1");
        mText.setAroundElements(span1, null, span2, null);
        assertEquals(span1.getDrawable(), mText.getLeftElement());
        assertEquals(span2.getDrawable(), mText.getRightElement());
        assertNotEquals(span1.getDrawable(), mText.getRightElement());
    }

    /**
     * test case ResetText
     */
    @Test
    public void testResetText() {
        String str1 = "HelloWorld1";
        String str2 = "HelloWorld2";
        mText.setText(str1);
        PixelMapElement pixelMapElement = ResUtil.getPixelMapDrawable(context, ResourceTable.Media_icon);
        DraweeSpan span = new DraweeSpan.Builder(
            "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(100, 100)
            .setPlaceHolderImage(pixelMapElement)
            .build();
        mText.setAroundElements(span, null, null, null);
        mText.setText(str2);
        assertEquals(mText.getText(), str2);
    }
}