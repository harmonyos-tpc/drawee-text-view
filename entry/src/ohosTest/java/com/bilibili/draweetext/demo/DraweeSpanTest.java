/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.draweetext.demo;

import static org.junit.Assert.assertEquals;

import com.bilibili.draweetext.DraweeSpan;

import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Rect;

import org.junit.Test;

/**
 * TestCases for DraweeSpan class
 */
public class DraweeSpanTest {
    /**
     * test case Build DraweeSpan
     */
    @Test
    public void testBuild() {
        Element ph = new ShapeElement();
        DraweeSpan span = new DraweeSpan.Builder("http://test").setLayout(100, 100).setPlaceHolderImage(ph).build();
        assertEquals("http://test", span.getImageUri());
        assertEquals(ph, span.getDrawable());
        assertEquals(new Rect(0, 0, 100, 100), span.getDrawable().getBounds());
    }
}
