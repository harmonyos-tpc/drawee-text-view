/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.draweetext.demo.sliderhelper;

import com.bilibili.draweetext.demo.util.LogUtil;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.PageSliderProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for PageSlider
 */
public class PageViewAdapter extends PageSliderProvider {
    private static final String TAG = PageViewAdapter.class.getCanonicalName();

    private AbilitySlice slice;

    private List<? extends PageInfo> pageViews;

    public PageViewAdapter(AbilitySlice abilitySlice, List<? extends PageInfo> list) {
        slice = abilitySlice;
        if (list != null) {
            pageViews = list;
            return;
        }
        pageViews = new ArrayList();
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object obj) {
        return true;
    }

    @Override
    public String getPageTitle(int position) {
        return "";
    }

    @Override
    public int getCount() {
        return pageViews.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        Component directionalLayout = new DirectionalLayout(slice);
        if (position >= 0 && position < pageViews.size()) {
            directionalLayout = pageViews.get(position).getRootView();
        }
        componentContainer.addComponent(directionalLayout);
        return directionalLayout;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int index, Object obj) {
        if (obj instanceof Component) {
            componentContainer.removeComponent((Component) obj);
        } else {
            if (componentContainer == null) {
                LogUtil.error(TAG, "destroy item failed, container is null");
            }
        }
    }
}
