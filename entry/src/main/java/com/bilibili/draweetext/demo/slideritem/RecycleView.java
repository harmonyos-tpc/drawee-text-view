/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.draweetext.demo.slideritem;

import com.bilibili.draweetext.demo.ResourceTable;
import com.bilibili.draweetext.demo.itemprovider.RecycleProvider;
import com.bilibili.draweetext.demo.slice.MainAbilitySlice;
import com.bilibili.draweetext.demo.sliderhelper.AbstractPageView;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;

/**
 * It helps to populate the RecycleView
 */
public class RecycleView extends AbstractPageView {
    public RecycleView(MainAbilitySlice abilitySlice) {
        super(abilitySlice);
    }

    @Override
    public void initComponent() {
        super.setRootView(loadView());
    }

    private Component loadView() {
        ComponentContainer layout = null;
        Component component = LayoutScatter.getInstance(super.getSlice())
            .parse(ResourceTable.Layout_page_two, null, false);
        if (component instanceof ComponentContainer) {
            layout = (ComponentContainer) component;
        }

        if (layout == null) {
            return layout;
        }

        component = layout.findComponentById(ResourceTable.Id_title);
        if (component instanceof Text) {
            Text title = (Text) component;
            title.setText("RecycleView");
        }

        component = layout.findComponentById(ResourceTable.Id_list_container);
        if (component instanceof ListContainer) {
            ListContainer listContainer = (ListContainer) component;
            RecycleProvider recycleProvider = new RecycleProvider(super.getSlice(), super.getListItems());
            listContainer.setItemProvider(recycleProvider);
        }
        return layout;
    }
}
