/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.draweetext.demo.sliderhelper;

import com.bilibili.draweetext.demo.util.LogUtil;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;

import java.util.ArrayList;

/**
 * Helper class for PageSlider
 */
public abstract class AbstractPageView implements PageInfo {
    private static final String TAG = AbstractPageView.class.getCanonicalName();

    private AbilitySlice slice;

    private Component rootView;

    public AbstractPageView(AbilitySlice abilitySlice) {
        if (abilitySlice != null) {
            slice = abilitySlice;
        } else {
            LogUtil.error(TAG, "slice is null, set default slice");
            slice = new AbilitySlice();
        }
        initComponent();
    }

    /**
     * initializes the component
     */
    public abstract void initComponent();

    /**
     * Obtains the slice instance
     *
     * @return instance of slice
     */
    public AbilitySlice getSlice() {
        return slice;
    }

    /**
     * Obtains the items for the listContainer
     *
     * @return list of string
     */
    public ArrayList<String> getListItems() {
        ArrayList<String> listItems = new ArrayList<>();
        listItems.add("https://i0.hdslb.com/bfs/vip/f80d384875183dfe2e24be13011c595c0210d273.png@.webp");
        listItems.add("https://i0.hdslb.com/bfs/vip/eb41a8c04840e4f77e76a4bff7a29ac89c432f4e.png@.webp");
        listItems.add("https://i0.hdslb.com/bfs/vip/16b8794be990cefa6caeba4d901b934a227ee3b8.png@.webp");
        listItems.add("https://i0.hdslb.com/bfs/vip/d1628c43d35b1530c0504a643ff80b6189fa0a43.png@.webp");
        listItems.add("https://i0.hdslb.com/bfs/vip/fdb5870f32cfaf7949e0f88a13f6feba4a48b719.png@.webp");
        listItems.add("https://i0.hdslb.com/bfs/vip/3754ee6e5985bd0bd7dfb668981f2a8733398ebd.png@.webp");
        listItems.add("https://i0.hdslb.com/bfs/vip/0b41f509351958dbb63d472fec0132d1bd03bd14.png@.webp");
        listItems.add("https://i0.hdslb.com/bfs/vip/6f058f78bce5d1c9b370c3807c891e685bb68a17.png");
        listItems.add("https://i0.hdslb.com/bfs/vip/7f482b82a3de44ae14537cbafcbc40cf65f7113e.png");
        listItems.add("https://i0.hdslb.com/bfs/vip/458982f20f0b7dc68c0ddac89f51ecb7c3d16a83.png");
        listItems.add("https://i0.hdslb.com/bfs/vip/6846363907204271f0a57472744642c8882b4019.png");
        listItems.add("https://i0.hdslb.com/bfs/vip/de3aee88f7b6cc20ba9480c96c02f83a844381a9.png");
        listItems.add("https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png");
        listItems.add("https://i0.hdslb.com/bfs/vip/a695fe1301aab2675ab6f6e34757c25a863a8617.png@.webp");
        listItems.add("https://i0.hdslb.com/bfs/vip/77545a5e420e2c43e0e4a7996a71769638ae3f90.png");
        listItems.add("https://i0.hdslb.com/bfs/vip/af8f017e383a1999e26a7f91c3ec3c83fbb7ba77.png");
        return listItems;
    }

    @Override
    public Component getRootView() {
        return rootView;
    }

    public void setRootView(Component component) {
        rootView = component;
    }
}