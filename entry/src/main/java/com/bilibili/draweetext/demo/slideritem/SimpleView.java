/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.draweetext.demo.slideritem;

import static com.bilibili.draweetext.demo.util.Constants.HEIGHT;
import static com.bilibili.draweetext.demo.util.Constants.WIDTH;

import com.bilibili.draweetext.DraweeSpan;
import com.bilibili.draweetext.DraweeTextView;
import com.bilibili.draweetext.demo.ResourceTable;
import com.bilibili.draweetext.demo.slice.MainAbilitySlice;
import com.bilibili.draweetext.demo.sliderhelper.AbstractPageView;
import com.bilibili.draweetext.demo.util.ResUtil;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;

/**
 * It helps to populate the simpleDraweeText
 */
public class SimpleView extends AbstractPageView {
    public SimpleView(MainAbilitySlice abilitySlice) {
        super(abilitySlice);
    }

    @Override
    public void initComponent() {
        super.setRootView(loadView());
    }

    private Component loadView() {
        Component component = LayoutScatter.getInstance(super.getSlice())
            .parse(ResourceTable.Layout_page_one, null, false);
        ComponentContainer layout = null;
        if (component instanceof ComponentContainer) {
            layout = (ComponentContainer) component;
        }

        if (layout == null) {
            return layout;
        }

        component = layout.findComponentById(ResourceTable.Id_title);
        if (component instanceof Text) {
            Text title = (Text) component;
            title.setText("Simple");
        }

        PixelMapElement pixelMapElement = ResUtil.getPixelMapDrawable(super.getSlice(), ResourceTable.Media_icon);
        DraweeSpan span = new DraweeSpan.Builder(
            "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(WIDTH, HEIGHT)
            .setPlaceHolderImage(pixelMapElement)
            .build();

        DraweeTextView text;
        component = layout.findComponentById(ResourceTable.Id_drawee_text);
        if (component instanceof DraweeTextView) {
            text = (DraweeTextView) component;
            text.setText("~~~~~~~~~~~~~~~Hello World~~~~~~~~~~~~~~~");
            text.setAroundElements(span, span, span, span);
        }
        return layout;
    }
}
