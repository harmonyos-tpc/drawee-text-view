/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.draweetext.demo.slice;

import com.bilibili.draweetext.demo.ResourceTable;
import com.bilibili.draweetext.demo.sliderhelper.AbstractPageView;
import com.bilibili.draweetext.demo.sliderhelper.PageViewAdapter;
import com.bilibili.draweetext.demo.slideritem.ListView;
import com.bilibili.draweetext.demo.slideritem.RecycleView;
import com.bilibili.draweetext.demo.slideritem.SimpleView;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;

import java.util.ArrayList;

/**
 * Constants
 */
public class MainAbilitySlice extends AbilitySlice {
    private PageSlider mPager;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_slider);
        Component component = findComponentById(ResourceTable.Id_slider);
        if (component instanceof PageSlider) {
            mPager = (PageSlider) component;
        }
        initPageView();
    }

    private void initPageView() {
        if (mPager == null) {
            return;
        }
        initPager();
        mPager.setOrientation(Component.HORIZONTAL);
        mPager.setSlidingPossible(true);
    }

    private void initPager() {
        ArrayList<AbstractPageView> pageViews = new ArrayList<>();
        pageViews.add(new SimpleView(this));
        pageViews.add(new ListView(this));
        pageViews.add(new RecycleView(this));
        PageViewAdapter pageViewAdapter = new PageViewAdapter(this, pageViews);
        mPager.setProvider(pageViewAdapter);
    }
}