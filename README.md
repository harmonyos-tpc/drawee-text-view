# drawee-text-view

drawee-text-view: Simple openharmony component for displaying the images around the Text component using Fresco.

# drawee-text-view includes :
* Displaying the image at the left, right,top and bottom of the Text component

# Usage Instructions

1. Add internet permission in config.json file.
```
    "reqPermissions": [
      {
        "reason": "get right",
        "name": "ohos.permission.INTERNET",
        "used-scene": {
          "ability": [
            "com.bilibili.draweetext.demo.ability.MainAbility"
          ],
          "when": "always"
        }
      }
    ]
```

2. Initialize the Fresco library in AbilityPackage class.
```
Fresco.initialize(this, ImagePipelineConfig.newBuilder(this).experiment().setWebpSupportEnabled(true).build());
```
3. drawee-text-view can be used by instantiating the layout elements programmatically as shown below :
```
<com.bilibili.draweetext.DraweeTextView
        ohos:id="$+id:drawee_text"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:layout_alignment="center"/>
```
Then set the image url and its dimension using DraweeSpan and set it to DraweeTextView :

```
ShapeElement shapeElement = new ShapeElement();
shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.BLUE.getValue()));
DraweeSpan span = new DraweeSpan.Builder(
            "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(100, 100)
            .setPlaceHolderImage(shapeElement)
            .build();

DraweeTextView text = (DraweeTextView) layout.findComponentById(ResourceTable.Id_drawee_text);
text.setText("~~~~~~~~~~~~~~~Hello World~~~~~~~~~~~~~~~");
text.setAroundElements(span, span, span, span);

```

# Installation Instructions

1. Copy the fresco so [(click here)](https://gitee.com/openharmony-tpc/fresco) to the libs folder of the project.

2. For using drawee-text-view module in sample app, include the source code and add the below dependencies to generate hap/draweetext.har.

Modify entry/build.gradle as below:
```
dependencies {
    implementation project(':draweetext’)
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation 'com.facebook.soloader:nativeloader:0.9.0'
    implementation ("com.parse.bolts:bolts-tasks:1.4.0")
    implementation 'io.openharmony.tpc.thirdlib:fresco:1.0.0'
}
```
Modify draweetext/build.gradle as below:
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation 'io.openharmony.tpc.thirdlib:DraweeTextView:1.0.1'
    implementation 'io.openharmony.tpc.thirdlib:fresco:1.0.0'
    implementation 'com.facebook.soloader:nativeloader:0.9.0'
    implementation ("com.parse.bolts:bolts-tasks:1.4.0")
}
```
3. For using drawee-text-view in separate application using har file, add the har file in the entry/libs folder and add the dependencies in entry/build.gradle file.

Modify entry/build.gradle as below :
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation 'com.facebook.soloader:nativeloader:0.9.0'
    implementation ("com.parse.bolts:bolts-tasks:1.4.0")
    implementation 'io.openharmony.tpc.thirdlib:fresco:1.0.0'
}
```

4. For using drawee-text-view from a remote repository in separate application, add the below dependencies in entry/build.gradle file.

    	```
    	dependencies {
    	    implementation fileTree(dir: 'libs', include: ['*.har'])
    	    implementation 'com.facebook.soloader:nativeloader:0.9.0'
            implementation ("com.parse.bolts:bolts-tasks:1.4.0")
    	    implementation 'io.openharmony.tpc.thirdlib:fresco:1.0.0'
    	    implementation 'io.openharmony.tpc.thirdlib:drawee-text-view:1.0.1'
    	}
        ```