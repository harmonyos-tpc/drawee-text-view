/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.draweetext.util;

/**
 * Constants
 */
public class Constants {
    /**
     * Constant for one minute
     */
    public static final int HEIGHT = 100;

    /**
     * Constant for one minute
     */
    public static final int WIDTH = 100;

    /**
     * Constant for one minute
     */
    public static final int INT_4 = 4;

    /**
     * Constant for one minute
     */
    public static final int INT_3 = 3;

    /**
     * Constant for one minute
     */
    public static final int INT_2 = 2;

    /**
     * Constant for one minute
     */
    public static final int INT_1 = 1;

    /**
     * Constant for one minute
     */
    public static final int INT_0 = 0;

    /**
     * Constant for one minute
     */
    public static final int INT_N_1 = -1;

    private Constants() {
    }
}

